package br.com.testegithub.testeconcrete.entity;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class Repo {
    private String name;
    private String description;
    private OwnerRepo owner;
    private String stargazers_count;
    private String forks;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public OwnerRepo getOwner() {
        return owner;
    }

    public String getStargazers_count() {
        return stargazers_count;
    }

    public String getForks() {
        return forks;
    }

    public class OwnerRepo {
        private String avatar_url;
        private String login;

        public String getAvatar_url() {
            return avatar_url;
        }

        public String getLogin() {
            return login;
        }
    }


}
