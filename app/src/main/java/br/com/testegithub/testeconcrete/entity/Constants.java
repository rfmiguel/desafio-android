package br.com.testegithub.testeconcrete.entity;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class Constants {
    public static final String CREATOR = "creator";
    public static final String REPO = "repo";
    public static final String EXCEPTION = "exception";
}
