package br.com.testegithub.testeconcrete.entity;

import java.util.List;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class PullRequestResponse {
    private List<PullRequest> pullRequestList;

    public List<PullRequest> getPullRequestList() {
        return pullRequestList;
    }
}
