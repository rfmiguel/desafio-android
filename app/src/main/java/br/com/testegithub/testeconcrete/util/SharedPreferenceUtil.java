package br.com.testegithub.testeconcrete.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.testegithub.testeconcrete.entity.PullRequest;
import br.com.testegithub.testeconcrete.entity.Repo;

/**
 * Created by rodrigo on 18/08/2016.
 */
public class SharedPreferenceUtil {

    private SharedPreferences mPrefs;
    private static final String KEY = "gitAppPreferences";
    private static final String INDEX_KEY = "indexKey";
    private static final String REPO_KEY = "repoKey";
    private SharedPreferences.Editor prefsEditor;
    private int page=1;

    public SharedPreferenceUtil(Context context) {
        this.mPrefs = context.getSharedPreferences(KEY,Context.MODE_PRIVATE);
        this.prefsEditor = this.mPrefs.edit();
        this.prefsEditor.apply();
    }

    public void saveRepoList(int index,List<Repo> repoList){
        Gson gson = new Gson();
        if(repoList!= null) {
            this.prefsEditor.putInt(INDEX_KEY, index);
            this.prefsEditor.putString(REPO_KEY,gson.toJson(repoList));
            this.prefsEditor.commit();
        }
    }

    public int getIndexRepo(){
        return this.mPrefs.getInt(INDEX_KEY,0);
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Repo> getRepoList(){
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Repo>>() {
        }.getType();
        return gson.fromJson(this.mPrefs.getString(REPO_KEY,""),listType);
    }
}
