package br.com.testegithub.testeconcrete;

import dagger.ObjectGraph;

/**
 * Created by rodrigo on 16/08/2016.
 */
public class TesteConcreteApplication extends android.app.Application {

    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        this.objectGraph = ObjectGraph.create(new TesteConcreteDaggerModule(this));
    }

    public ObjectGraph getObjectGraph() {
        return this.objectGraph;
    }

}
