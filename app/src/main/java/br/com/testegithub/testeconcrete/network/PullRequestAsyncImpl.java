package br.com.testegithub.testeconcrete.network;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.testegithub.testeconcrete.BuildConfig;
import br.com.testegithub.testeconcrete.entity.Constants;
import br.com.testegithub.testeconcrete.entity.PullRequest;
import br.com.testegithub.testeconcrete.entity.PullRequestAsyncResult;
import br.com.testegithub.testeconcrete.entity.ServiceResult;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class PullRequestAsyncImpl implements PullRequestAsync,OnTransformResult<PullRequestAsyncResult>{

    private final ServiceClient<PullRequestAsyncResult> serviceClient;
    public static final String URL_SEARCH ="/repos/%s/%s/pulls?page=%d";

    @Inject
    public PullRequestAsyncImpl(ServiceClient<PullRequestAsyncResult> serviceClient ) {
        this.serviceClient = serviceClient;
    }

    @Override
    public void getPullRequest(int page,String creator, String repo, OnServiceCall<PullRequestAsyncResult> onServiceCall) {
        this.serviceClient.get(BuildConfig.URL_GITHUB + String.format(URL_SEARCH,creator,repo,page),this,onServiceCall);
    }

    @Override
    public PullRequestAsyncResult onTransformResult(String resultResponse) {
        try {
            List<PullRequest> response;
            Type listType = new TypeToken<ArrayList<PullRequest>>() {
            }.getType();
            response = new Gson().fromJson(resultResponse, listType);
            return new PullRequestAsyncResult(ServiceResult.SUCCESS,response);

        } catch (Exception e) {
            return new PullRequestAsyncResult(ServiceResult.FAIL, Constants.EXCEPTION);
        }
    }
}
