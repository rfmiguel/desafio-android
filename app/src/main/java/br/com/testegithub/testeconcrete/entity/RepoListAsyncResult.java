package br.com.testegithub.testeconcrete.entity;

import java.util.List;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class RepoListAsyncResult {
    private ServiceResult serviceResult;
    private List<Repo> repoList;
    private String message;

    public RepoListAsyncResult(ServiceResult serviceResult, List<Repo> repoList) {
        this.serviceResult = serviceResult;
        this.repoList = repoList;
    }

    public RepoListAsyncResult(ServiceResult serviceResult, String message) {
        this.serviceResult = serviceResult;
        this.message = message;
    }

    public ServiceResult getServiceResult() {
        return serviceResult;
    }

    public List<Repo> getRepoList() {
        return repoList;
    }

    public String getMessage() {
        return message;
    }
}
