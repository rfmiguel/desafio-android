package br.com.testegithub.testeconcrete.model;

import java.util.List;

import br.com.testegithub.testeconcrete.entity.PullRequest;
import br.com.testegithub.testeconcrete.model.delegate.PullRequestModelDelegate;

/**
 * Created by rodrigo on 17/08/2016.
 */
public interface PullRequestModel {
    void getPullRequest(String creator,String repo,int page);
    void setPullRequestModelDelegate(PullRequestModelDelegate pullRequestModelDelegate);
    List<PullRequest> getRepoList();
}
