package br.com.testegithub.testeconcrete.controller.adapter;

/**
 * Created by rodrigo on 17/08/2016.
 */
public interface OnLoadMoreListener {
    void onLoadNextPage(int page);
}
