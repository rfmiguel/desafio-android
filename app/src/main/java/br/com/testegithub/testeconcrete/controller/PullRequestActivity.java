package br.com.testegithub.testeconcrete.controller;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import javax.inject.Inject;

import br.com.testegithub.testeconcrete.R;
import br.com.testegithub.testeconcrete.controller.adapter.OnItemClickListener;
import br.com.testegithub.testeconcrete.controller.adapter.OnLoadMoreListener;
import br.com.testegithub.testeconcrete.controller.adapter.PullRequestListAdapter;
import br.com.testegithub.testeconcrete.controller.adapter.RecycleViewScrollListener;
import br.com.testegithub.testeconcrete.entity.Constants;
import br.com.testegithub.testeconcrete.entity.ErrorTypes;
import br.com.testegithub.testeconcrete.entity.PullRequest;
import br.com.testegithub.testeconcrete.model.PullRequestModel;
import br.com.testegithub.testeconcrete.model.delegate.PullRequestModelDelegate;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class PullRequestActivity extends BaseActivity implements PullRequestModelDelegate,OnLoadMoreListener,OnItemClickListener<PullRequest> {

    private RecyclerView rvList;
    private PullRequestListAdapter pullRequestListAdapter;
    private RecycleViewScrollListener recycleViewScrollListener;
    private String creator;
    private String repo;
    private Toolbar toolbar;
    private ProgressBar progressBar;

    @Inject
    PullRequestModel pullRequestModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pullrequest);

        this.toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        this.setSupportActionBar(this.toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PullRequestActivity.this.finish();
            }
        });

        this.rvList = (RecyclerView)this.findViewById(R.id.activity_pullrequest_rv_listpullrequest);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        this.recycleViewScrollListener = new RecycleViewScrollListener(layoutManager,1,this);
        this.rvList.addOnScrollListener(this.recycleViewScrollListener);
        this.rvList.setLayoutManager(layoutManager);

        this.pullRequestModel.setPullRequestModelDelegate(this);

        this.creator = this.getIntent().getExtras().getString(Constants.CREATOR);
        this.repo = this.getIntent().getExtras().getString(Constants.REPO);

        this.progressBar = (ProgressBar)this.findViewById(R.id.activity_pullrequest_pb);

        this.pullRequestModel.getPullRequest(creator,repo,1);
    }

    @Override
    public void onGetPullRequestOK() {

        this.progressBar.setVisibility(View.GONE);

        if(this.pullRequestListAdapter == null) {
            this.pullRequestListAdapter = new PullRequestListAdapter(this,this.pullRequestModel.getRepoList(),this);
            this.rvList.setAdapter(this.pullRequestListAdapter);
        }else{
            this.pullRequestListAdapter.updateDataSet(this.pullRequestModel.getRepoList());
        }
    }

    @Override
    public void onGetPullRequestFail(ErrorTypes errorTypes) {
        this.progressBar.setVisibility(View.GONE);

        Toast.makeText(PullRequestActivity.this, errorTypes.message(this), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoadNextPage(int page) {
        this.pullRequestModel.getPullRequest(this.creator,this.repo,page);
    }

    @Override
    public void onItemClick(PullRequest item) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getUrl()));
        startActivity(browserIntent);
    }
}
