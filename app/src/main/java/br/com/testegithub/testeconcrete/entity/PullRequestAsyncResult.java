package br.com.testegithub.testeconcrete.entity;

import java.util.List;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class PullRequestAsyncResult {
    private ServiceResult serviceResult;
    private List<PullRequest> pullRequestList;
    private String message;

    public PullRequestAsyncResult(ServiceResult serviceResult, List<PullRequest> pullRequestList) {
        this.serviceResult = serviceResult;
        this.pullRequestList = pullRequestList;
    }

    public PullRequestAsyncResult(ServiceResult serviceResult, String message) {
        this.serviceResult = serviceResult;
        this.message = message;
    }

    public ServiceResult getServiceResult() {
        return serviceResult;
    }

    public List<PullRequest> getPullRequestList() {
        return pullRequestList;
    }

    public String getMessage() {
        return message;
    }
}
