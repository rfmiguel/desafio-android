package br.com.testegithub.testeconcrete.model;

import java.util.List;

import br.com.testegithub.testeconcrete.entity.BusinessErrors;
import br.com.testegithub.testeconcrete.entity.ErrorTypes;
import br.com.testegithub.testeconcrete.entity.Repo;
import br.com.testegithub.testeconcrete.entity.RepoListAsyncResult;
import br.com.testegithub.testeconcrete.model.delegate.RepoModelDelegate;
import br.com.testegithub.testeconcrete.network.OnServiceCall;
import br.com.testegithub.testeconcrete.network.RepoListAsync;
import br.com.testegithub.testeconcrete.entity.ServiceResult;

/**
 * Created by rodrigo on 16/08/2016.
 */
public class RepoModelImpl implements RepoModel , OnServiceCall<RepoListAsyncResult> {

    private RepoListAsync repoListAsync;
    private RepoModelDelegate repoModelDelegate;
    private List<Repo> repoList;

    public RepoModelImpl(RepoListAsync repoListAsync) {
        this.repoListAsync = repoListAsync;
    }

    @Override
    public void getRepos(int page) {
        this.repoListAsync.getRepoList(page,this);
    }

    @Override
    public void setRepoModelDelegate(RepoModelDelegate repoModelDelegate) {
        this.repoModelDelegate = repoModelDelegate;
    }

    @Override
    public List<Repo> getRepoList() {
        return this.repoList;
    }

    @Override
    public void onServiceCallSuccess(RepoListAsyncResult serviceResult) {
        this.repoList = serviceResult.getRepoList();
        if(serviceResult.getServiceResult().equals(ServiceResult.SUCCESS)){
            this.repoModelDelegate.onGetReposOK();
        } else{
            this.repoModelDelegate.onGetReposFail(new BusinessErrors(serviceResult.getMessage()));
        }
    }

    @Override
    public void onServiceCallFail(ErrorTypes errorTypes) {
        this.repoModelDelegate.onGetReposFail(errorTypes);
    }
}
