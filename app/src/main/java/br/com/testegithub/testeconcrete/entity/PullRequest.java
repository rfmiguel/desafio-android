package br.com.testegithub.testeconcrete.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class PullRequest {
    private String title;
    private String body;
    private User user;
    private String created_at;
    @SerializedName("html_url")
    private String url;

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public User getUser() {
        return user;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUrl() {
        return url;
    }
}
