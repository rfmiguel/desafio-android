package br.com.testegithub.testeconcrete.model;

import java.util.List;

import br.com.testegithub.testeconcrete.entity.BusinessErrors;
import br.com.testegithub.testeconcrete.entity.ErrorTypes;
import br.com.testegithub.testeconcrete.entity.PullRequest;
import br.com.testegithub.testeconcrete.entity.PullRequestAsyncResult;
import br.com.testegithub.testeconcrete.entity.ServiceResult;
import br.com.testegithub.testeconcrete.model.delegate.PullRequestModelDelegate;
import br.com.testegithub.testeconcrete.network.OnServiceCall;
import br.com.testegithub.testeconcrete.network.PullRequestAsync;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class PullRequestModelmpl implements PullRequestModel, OnServiceCall<PullRequestAsyncResult>{

    private PullRequestAsync pullRequestAsync;
    private PullRequestModelDelegate pullRequestModelDelegate;
    private List<PullRequest> pullRequests;

    public PullRequestModelmpl(PullRequestAsync pullRequestAsync) {
        this.pullRequestAsync = pullRequestAsync;
    }

    @Override
    public void getPullRequest(String creator, String repo,int page) {
        this.pullRequestAsync.getPullRequest(page,creator,repo,this);
    }

    @Override
    public void setPullRequestModelDelegate(PullRequestModelDelegate pullRequestModelDelegate) {
        this.pullRequestModelDelegate = pullRequestModelDelegate;
    }

    @Override
    public List<PullRequest> getRepoList() {
        return this.pullRequests;
    }

    @Override
    public void onServiceCallSuccess(PullRequestAsyncResult serviceResult) {
        this.pullRequests = serviceResult.getPullRequestList();
        if(serviceResult.getServiceResult().equals(ServiceResult.SUCCESS)){
            this.pullRequestModelDelegate.onGetPullRequestOK();
        } else{
            this.pullRequestModelDelegate.onGetPullRequestFail(new BusinessErrors(serviceResult.getMessage()));
        }
    }

    @Override
    public void onServiceCallFail(ErrorTypes errorTypes) {
        this.pullRequestModelDelegate.onGetPullRequestFail(errorTypes);
    }
}
