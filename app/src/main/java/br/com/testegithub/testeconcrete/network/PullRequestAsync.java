package br.com.testegithub.testeconcrete.network;

import br.com.testegithub.testeconcrete.entity.PullRequestAsyncResult;

/**
 * Created by rodrigo on 17/08/2016.
 */
public interface PullRequestAsync {
    void getPullRequest(int page,String creator, String repo, OnServiceCall<PullRequestAsyncResult> onServiceCall);
}
