package br.com.testegithub.testeconcrete.network;

import br.com.testegithub.testeconcrete.entity.RepoListAsyncResult;

/**
 * Created by rodrigo on 17/08/2016.
 */
public interface RepoListAsync {
    void getRepoList(int page,OnServiceCall<RepoListAsyncResult> onServiceCall);
}
