package br.com.testegithub.testeconcrete.entity;

import android.content.Context;

import br.com.testegithub.testeconcrete.R;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class BusinessErrors implements ErrorTypes {

    private static final String EXCEPTION = "exception";
    private String errorMessage;

    public BusinessErrors(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String message(Context context) {
        if (this.errorMessage.equals(EXCEPTION)) {
            return context.getString(R.string.unexpected_error);
        }
        return this.errorMessage;
    }
}
