package br.com.testegithub.testeconcrete.controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import br.com.testegithub.testeconcrete.TesteConcreteApplication;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TesteConcreteApplication application = (TesteConcreteApplication) this.getApplication();
        application.getObjectGraph().inject(this);
    }
}

