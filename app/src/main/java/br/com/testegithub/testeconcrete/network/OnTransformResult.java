package br.com.testegithub.testeconcrete.network;

/**
 * Created by rodrigo on 17/08/2016.
 */
public interface OnTransformResult<T> {
    T onTransformResult(String resultResponse);
}
