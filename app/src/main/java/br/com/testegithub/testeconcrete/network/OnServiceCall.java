package br.com.testegithub.testeconcrete.network;

import br.com.testegithub.testeconcrete.entity.ErrorTypes;

/**
 * Created by rodrigo on 17/08/2016.
 */
public interface OnServiceCall<T> {
    void onServiceCallSuccess(T serviceResult);

    void onServiceCallFail(ErrorTypes errorTypes);
}
