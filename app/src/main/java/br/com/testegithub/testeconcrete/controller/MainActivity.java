package br.com.testegithub.testeconcrete.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import javax.inject.Inject;

import br.com.testegithub.testeconcrete.R;
import br.com.testegithub.testeconcrete.controller.adapter.OnItemClickListener;
import br.com.testegithub.testeconcrete.controller.adapter.OnLoadMoreListener;
import br.com.testegithub.testeconcrete.controller.adapter.RecycleViewScrollListener;
import br.com.testegithub.testeconcrete.controller.adapter.RepoListAdapter;
import br.com.testegithub.testeconcrete.entity.Constants;
import br.com.testegithub.testeconcrete.entity.ErrorTypes;
import br.com.testegithub.testeconcrete.entity.Repo;
import br.com.testegithub.testeconcrete.model.RepoModel;
import br.com.testegithub.testeconcrete.model.delegate.RepoModelDelegate;
import br.com.testegithub.testeconcrete.util.SharedPreferenceUtil;

public class MainActivity extends BaseActivity implements RepoModelDelegate,OnLoadMoreListener,OnItemClickListener<Repo>{

    private RecyclerView rvList;
    private RepoListAdapter repoListAdapter;
    private RecycleViewScrollListener recycleViewScrollListener;
    private Toolbar toolbar;
    private ProgressBar progressBar;
    private LinearLayoutManager layoutManager;

    @Inject
    SharedPreferenceUtil sharedPreferenceUtil;

    @Inject
    RepoModel repoModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.toolbar = (Toolbar) this.findViewById(R.id.activity_main_toolbar);
        this.setSupportActionBar(this.toolbar);
        this.getSupportActionBar().setTitle("GitHub JavaPop");

        this.rvList = (RecyclerView)findViewById(R.id.activity_main_rv_listgit);
        this.layoutManager = new LinearLayoutManager(this);
        this.layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        this.recycleViewScrollListener = new RecycleViewScrollListener(layoutManager,this.sharedPreferenceUtil.getPage(),this);
        this.rvList.addOnScrollListener(this.recycleViewScrollListener);
        this.rvList.setLayoutManager(layoutManager);
        this.repoListAdapter = new RepoListAdapter(this,this);
        this.rvList.setAdapter(this.repoListAdapter);

        this.progressBar = (ProgressBar) findViewById(R.id.activity_main_pb);

        this.repoModel.setRepoModelDelegate(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(this.repoModel.getRepoList() == null){
            this.repoModel.getRepos(1);
        }else{
            this.progressBar.setVisibility(View.GONE);
            this.repoListAdapter.updateDataSet(this.sharedPreferenceUtil.getRepoList());
            this.layoutManager.scrollToPosition(this.sharedPreferenceUtil.getIndexRepo());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.sharedPreferenceUtil.saveRepoList(this.repoListAdapter.getPosition(),
                this.repoListAdapter.getRepoList());
    }

    @Override
    public void onGetReposOK() {
        this.progressBar.setVisibility(View.GONE);
        this.repoListAdapter.updateDataSet(this.repoModel.getRepoList());
    }

    @Override
    public void onGetReposFail(ErrorTypes errorTypes) {
        this.progressBar.setVisibility(View.GONE);
        Toast.makeText(MainActivity.this, errorTypes.message(this), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onLoadNextPage(int page) {
        this.sharedPreferenceUtil.setPage(page);
        this.repoModel.getRepos(page);
    }

    @Override
    public void onItemClick(Repo item) {
        Log.d("MainActivity",item.getOwner().getLogin() + "/" + item.getName());
        Intent intent = new Intent(this,PullRequestActivity.class);
        intent.putExtra(Constants.CREATOR,item.getOwner().getLogin());
        intent.putExtra(Constants.REPO,item.getName());
        startActivity(intent);
    }
}
