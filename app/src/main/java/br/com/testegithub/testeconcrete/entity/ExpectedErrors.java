package br.com.testegithub.testeconcrete.entity;

import android.content.Context;

import br.com.testegithub.testeconcrete.R;
import br.com.testegithub.testeconcrete.entity.ErrorTypes;

/**
 * Created by rodrigo on 17/08/2016.
 */
public enum ExpectedErrors implements ErrorTypes {

    ERRO_INESPERADO {
        @Override
        public String message(Context context) {
            return context.getString(R.string.unexpected_error);
        }
    },

    SEM_CONEXAO {
        @Override
        public String message(Context context) {
            return context.getString(R.string.error_sem_conexao);
        }
    },
}
