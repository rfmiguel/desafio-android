package br.com.testegithub.testeconcrete.entity;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class User {
    private String login;
    private String avatar_url;

    public String getLogin() {
        return login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }
}
