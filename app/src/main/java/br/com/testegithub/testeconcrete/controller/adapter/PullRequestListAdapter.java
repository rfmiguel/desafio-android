package br.com.testegithub.testeconcrete.controller.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.testegithub.testeconcrete.R;
import br.com.testegithub.testeconcrete.entity.PullRequest;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class PullRequestListAdapter extends RecyclerView.Adapter<PullRequestListAdapter.ListItemViewHolder>{

    private Context context;
    private List<PullRequest> pullRequestList;
    private OnItemClickListener<PullRequest> onItemClickListener;

    public PullRequestListAdapter(Context context, List<PullRequest> pullRequestList, OnItemClickListener<PullRequest> pullRequestOnItemClickListener) {
        this.context = context;
        this.pullRequestList = pullRequestList;
        this.onItemClickListener = pullRequestOnItemClickListener;
    }


    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.row_listpullrequest,
                        parent,
                        false);
        this.context = parent.getContext();
        return new ListItemViewHolder(itemView);
    }

    @Override
    public void onViewAttachedToWindow(ListItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        PullRequest pullRequest = this.pullRequestList.get(position);
        holder.title.setText(pullRequest.getTitle());
        holder.description.setText(pullRequest.getBody());

        Picasso.with(this.context)
                .load(pullRequest.getUser().getAvatar_url())
                .fit()
                .centerCrop()
                .into(holder.img);

        holder.username.setText(pullRequest.getUser().getLogin());
        holder.data.setText(String.format("criado em : %s",pullRequest.getCreated_at()));
    }

    public void updateDataSet(List<PullRequest> pullRequestList){
        this.pullRequestList.addAll(pullRequestList);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.pullRequestList.size();
    }

    public class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title;
        public TextView description;
        public ImageView img;
        public TextView username;
        public TextView data;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.row_listpullrequest_tv_title);
            this.description = (TextView) itemView.findViewById(R.id.row_listpullrequest_tv_desc);
            this.username = (TextView) itemView.findViewById(R.id.row_listpullrequest_tv_username);
            this.data = (TextView) itemView.findViewById(R.id.row_listpullrequest_tv_data);
            this.img = (ImageView) itemView.findViewById(R.id.row_listpullrequest_iv_imgpull);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            PullRequestListAdapter.this.onItemClickListener.onItemClick(PullRequestListAdapter.this.pullRequestList.get(getAdapterPosition()));
        }
    }
}