package br.com.testegithub.testeconcrete.controller.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.testegithub.testeconcrete.R;
import br.com.testegithub.testeconcrete.entity.Repo;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class RepoListAdapter extends RecyclerView.Adapter<RepoListAdapter.ListItemViewHolder>{

    private Context context;
    private List<Repo> repoList = new ArrayList<>();
    private OnItemClickListener<Repo> repoOnItemClickListener;
    private int position;

    public RepoListAdapter(Context context,OnItemClickListener<Repo> repoOnItemClickListener) {
        this.context = context;
        this.repoOnItemClickListener = repoOnItemClickListener;
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.row_listgithub,
                        parent,
                        false);
        this.context = parent.getContext();
        return new ListItemViewHolder(itemView);
    }

    @Override
    public void onViewAttachedToWindow(ListItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        this.position = position;
        Repo repo = this.repoList.get(position);
        holder.name.setText(repo.getName());
        holder.description.setText(repo.getDescription());
        holder.forks.setText(repo.getForks());
        holder.stars.setText(repo.getStargazers_count());

        Picasso.with(this.context)
                    .load(repo.getOwner().getAvatar_url())
                    .fit()
                    .centerCrop()
                    .into(holder.imgRepo);

        Typeface font = Typeface.createFromAsset(this.context.getAssets(), "fontawesome-webfont.ttf");
        holder.imgFork.setTypeface(font);
        holder.imgStar.setTypeface(font);

        holder.imgFork.setText(this.context.getString(R.string.ic_fork));
        holder.imgStar.setText(this.context.getString(R.string.ic_star));

        holder.username.setText(repo.getOwner().getLogin());
    }

    public void updateDataSet(List<Repo> repoList){
        this.repoList.addAll(repoList);
        this.notifyDataSetChanged();
    }

    public int getPosition(){
        return this.position;
    }

    @Override
    public int getItemCount() {
        return this.repoList.size();
    }

    public List<Repo> getRepoList(){
        return this.repoList;
    }

    public class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView name;
        public TextView description;
        public TextView forks;
        public ImageView imgRepo;
        public TextView stars;
        public TextView username;
        public TextView imgFork;
        public TextView imgStar;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.row_listgithub_tv_name);
            this.description = (TextView) itemView.findViewById(R.id.row_listgithub_tv_desc);
            this.forks = (TextView) itemView.findViewById(R.id.row_listgithub_tv_fork);
            this.stars = (TextView) itemView.findViewById(R.id.row_listgithub_tv_star);
            this.imgRepo = (ImageView) itemView.findViewById(R.id.row_listgithub_iv_imgrepo);
            this.username = (TextView) itemView.findViewById(R.id.row_listgithub_tv_username);
            this.imgFork = (TextView) itemView.findViewById(R.id.row_listgithub_iv_fork);
            this.imgStar = (TextView) itemView.findViewById(R.id.row_listgithub_iv_star);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            RepoListAdapter.this.repoOnItemClickListener.onItemClick(RepoListAdapter.this.repoList.get(getAdapterPosition()));
        }

        public int getPositionRecycleView(){
            return this.getAdapterPosition();
        }

    }
}
