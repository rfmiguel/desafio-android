package br.com.testegithub.testeconcrete.model.delegate;

import br.com.testegithub.testeconcrete.entity.ErrorTypes;

/**
 * Created by rodrigo on 17/08/2016.
 */
public interface PullRequestModelDelegate {
    void onGetPullRequestOK();
    void onGetPullRequestFail(ErrorTypes errorTypes);
}
