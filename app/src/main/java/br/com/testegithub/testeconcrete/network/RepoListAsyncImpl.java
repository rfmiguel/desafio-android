package br.com.testegithub.testeconcrete.network;

import com.google.gson.Gson;

import javax.inject.Inject;

import br.com.testegithub.testeconcrete.BuildConfig;
import br.com.testegithub.testeconcrete.entity.Constants;
import br.com.testegithub.testeconcrete.entity.ServiceResult;
import br.com.testegithub.testeconcrete.entity.RepoListAsyncResult;
import br.com.testegithub.testeconcrete.entity.RepoListResponse;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class RepoListAsyncImpl implements RepoListAsync, OnTransformResult<RepoListAsyncResult>{
    private final ServiceClient<RepoListAsyncResult> serviceClient;
    public static final String URL_SEARCH ="/search/repositories?q=language:Java&sort=stars&page=%d";

    @Inject
    public RepoListAsyncImpl(ServiceClient<RepoListAsyncResult> serviceClient) {
        this.serviceClient = serviceClient;
    }

    @Override
    public void getRepoList(int page,OnServiceCall<RepoListAsyncResult> onServiceCall) {
        this.serviceClient.get(BuildConfig.URL_GITHUB + String.format(URL_SEARCH,page),this,onServiceCall);
    }

    @Override
    public RepoListAsyncResult onTransformResult(String resultResponse) {
        try {
            RepoListResponse response = new Gson().fromJson(resultResponse, RepoListResponse.class);
            return new RepoListAsyncResult(ServiceResult.SUCCESS,response.getItems());

        } catch (Exception e) {
            return new RepoListAsyncResult(ServiceResult.FAIL, Constants.EXCEPTION);
        }
    }
}
