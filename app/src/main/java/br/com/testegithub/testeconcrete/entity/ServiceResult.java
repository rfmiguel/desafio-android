package br.com.testegithub.testeconcrete.entity;

/**
 * Created by rodrigo on 17/08/2016.
 */
public enum ServiceResult {
    SUCCESS, FAIL
}
