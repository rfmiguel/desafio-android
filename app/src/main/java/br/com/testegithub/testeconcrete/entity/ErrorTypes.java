package br.com.testegithub.testeconcrete.entity;

import android.content.Context;

/**
 * Created by rodrigo on 17/08/2016.
 */
public interface ErrorTypes {
    String message(Context context);
}
