package br.com.testegithub.testeconcrete.network;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

import br.com.testegithub.testeconcrete.entity.ExpectedErrors;

public class ServiceClient<T> implements Callback {

    private OkHttpClient client;
    private OnServiceCall<T> onServiceCall;
    private OnTransformResult<T> onTransformResult;
    private String TAG = "ServiceClient";

    public ServiceClient(OkHttpClient client) {
        this.client = client;
    }

    public void get(String url, OnTransformResult<T> onTransformResult, OnServiceCall<T> onServiceCall) {
        this.onTransformResult = onTransformResult;
        this.onServiceCall = onServiceCall;
        Log.d(this.TAG,url);
        this.call("GET", url, null);
    }

    private void call(String method, String url, RequestBody requestBody) {
        Request.Builder builder = new Request.Builder();

        Request request = builder.url(url).method(method, method.equals("GET") ? null : requestBody)
                .build();

        this.client.newCall(request).enqueue(this);

    }

    @Override
    public void onFailure(Request request, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                ServiceClient.this.onServiceCall.onServiceCallFail(ExpectedErrors.SEM_CONEXAO);
            }
        });
    }

    @Override
    public void onResponse(final Response response) throws IOException {
        if (response.isSuccessful()) {
            final T result = ServiceClient.this.onTransformResult.onTransformResult(response.body().string());
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    ServiceClient.this.onServiceCall.onServiceCallSuccess(result);
                }
            });
        } else {
            final ExpectedErrors finalMessage = ExpectedErrors.ERRO_INESPERADO;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    ServiceClient.this.onServiceCall.onServiceCallFail(finalMessage);
                }
            });
        }
    }
}
