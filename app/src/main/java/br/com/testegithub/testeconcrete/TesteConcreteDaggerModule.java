package br.com.testegithub.testeconcrete;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import br.com.testegithub.testeconcrete.controller.MainActivity;
import br.com.testegithub.testeconcrete.controller.PullRequestActivity;
import br.com.testegithub.testeconcrete.entity.PullRequestAsyncResult;
import br.com.testegithub.testeconcrete.entity.RepoListAsyncResult;
import br.com.testegithub.testeconcrete.model.PullRequestModel;
import br.com.testegithub.testeconcrete.model.PullRequestModelmpl;
import br.com.testegithub.testeconcrete.model.RepoModel;
import br.com.testegithub.testeconcrete.model.RepoModelImpl;
import br.com.testegithub.testeconcrete.network.PullRequestAsync;
import br.com.testegithub.testeconcrete.network.PullRequestAsyncImpl;
import br.com.testegithub.testeconcrete.network.RepoListAsync;
import br.com.testegithub.testeconcrete.network.RepoListAsyncImpl;
import br.com.testegithub.testeconcrete.network.ServiceClient;
import br.com.testegithub.testeconcrete.util.SharedPreferenceUtil;
import dagger.Module;
import dagger.Provides;

/**
 * Created by rodrigo on 16/08/2016.
 */
@Module(injects = { MainActivity.class,
        PullRequestActivity.class })
public class TesteConcreteDaggerModule {

    private final TesteConcreteApplication testeConcreteApplication;

    public TesteConcreteDaggerModule(TesteConcreteApplication testeConcreteApplication) {
        this.testeConcreteApplication = testeConcreteApplication;
    }

    @Provides
    @Singleton
    RepoModel provideRepoModel(RepoListAsync repoListAsync){
        return new RepoModelImpl(repoListAsync);
    }

    @Provides
    @Singleton
    RepoListAsync provideRepoListAsync(ServiceClient<RepoListAsyncResult> serviceClient){
        return new RepoListAsyncImpl(serviceClient);
    }

    @Provides
    ServiceClient<RepoListAsyncResult> provideRepoListAsyncResultServiceClient(OkHttpClient okHttpClient) {
        return new ServiceClient<>(okHttpClient);
    }

    @Provides
    @Singleton
    PullRequestModel providePullRequestModel(PullRequestAsync pullRequestAsync){
        return new PullRequestModelmpl(pullRequestAsync);
    }

    @Provides
    @Singleton
    PullRequestAsync providePullRequestAsync(ServiceClient<PullRequestAsyncResult> serviceClient){
        return new PullRequestAsyncImpl(serviceClient);
    }

    @Provides
    ServiceClient<PullRequestAsyncResult> providePullRequestAsyncResultServiceClient(OkHttpClient okHttpClient) {
        return new ServiceClient<>(okHttpClient);
    }

    @Provides
    Context provideApplicationContext() {
        return this.testeConcreteApplication;
    }

    @Provides
    @Singleton
    SharedPreferenceUtil provideSharePreferenceUtil(Context context){
        return new SharedPreferenceUtil(context);
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient() {
        return new OkHttpClient();
    }
}
