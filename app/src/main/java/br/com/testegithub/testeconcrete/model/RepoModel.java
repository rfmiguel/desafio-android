package br.com.testegithub.testeconcrete.model;

import java.util.List;

import br.com.testegithub.testeconcrete.entity.Repo;
import br.com.testegithub.testeconcrete.model.delegate.RepoModelDelegate;

/**
 * Created by rodrigo on 16/08/2016.
 */
public interface RepoModel {
    void getRepos(int page);
    void setRepoModelDelegate(RepoModelDelegate repoModelDelegate);
    List<Repo> getRepoList();
}
