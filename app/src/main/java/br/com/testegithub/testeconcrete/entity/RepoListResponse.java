package br.com.testegithub.testeconcrete.entity;

import java.util.List;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class RepoListResponse {
    public List<Repo> items;

    public List<Repo> getItems() {
        return items;
    }
}
