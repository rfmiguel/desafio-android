package br.com.testegithub.testeconcrete.controller.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by rodrigo on 17/08/2016.
 */
public class RecycleViewScrollListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager mLinearLayoutManager;
    private int totalItemCount;
    private int visibleItemCount;
    private int firstVisibleItem;
    private boolean loading;
    private int startPage;
    private int visibleThreshold = 4;
    private int currentPage = 1;
    private int previousTotalItemCount = 0;

    private OnLoadMoreListener onLoadMoreListener;

    public RecycleViewScrollListener(LinearLayoutManager linearLayoutManager, int startPage, OnLoadMoreListener onLoadMoreListener) {
        this.mLinearLayoutManager = linearLayoutManager;
        this.startPage = startPage;
        this.currentPage = startPage;
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

        totalItemCount = this.mLinearLayoutManager.getItemCount();
        visibleItemCount = this.mLinearLayoutManager.getChildCount();
        firstVisibleItem  = this.mLinearLayoutManager.findFirstVisibleItemPosition();

        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startPage;
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) { this.loading = true; }
        }

        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
            currentPage++;
        }

        if (!loading && (firstVisibleItem + visibleItemCount + visibleThreshold) >= totalItemCount ) {
            this.onLoadMoreListener.onLoadNextPage(currentPage + 1);
            loading = true;
        }

    }

}
