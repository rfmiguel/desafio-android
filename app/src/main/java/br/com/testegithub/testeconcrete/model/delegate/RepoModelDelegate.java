package br.com.testegithub.testeconcrete.model.delegate;

import br.com.testegithub.testeconcrete.entity.ErrorTypes;

/**
 * Created by rodrigo on 17/08/2016.
 */
public interface RepoModelDelegate {
    void onGetReposOK();
    void onGetReposFail(ErrorTypes errorTypes);
}
