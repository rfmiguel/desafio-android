package br.com.testegithub.testeconcrete;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

import br.com.testegithub.testeconcrete.controller.PullRequestActivity;
import br.com.testegithub.testeconcrete.entity.Constants;

/**
 * Created by rodrigo on 18/08/2016.
 */
public class PullRequestActivityTest extends ActivityInstrumentationTestCase2<PullRequestActivity> {

    private Solo solo;

    public PullRequestActivityTest() {
        super(PullRequestActivity.class);
    }

    public void setUp() throws Exception {
        Intent i = new Intent();
        i.putExtra(Constants.CREATOR, "facebook");
        i.putExtra(Constants.REPO,"react-native");
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
    }


    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }

    public void testPullRequestActivity() throws Exception {
        solo.assertCurrentActivity("pull request activity", PullRequestActivity.class);
    }

    public void testListItemClickshouldOpenBrowser() throws Exception{
        RecyclerView myRecyclerView = (RecyclerView) solo.getView(R.id.activity_pullrequest_rv_listpullrequest);
        solo.waitForView(myRecyclerView);
        solo.clickInRecyclerView(0);
    }

    public void testListItemScroll() throws Exception {
        RecyclerView myRecyclerView = (RecyclerView) solo.getView(R.id.activity_pullrequest_rv_listpullrequest);
        solo.waitForView(myRecyclerView);
        solo.scrollDownRecyclerView(2);
    }
}
