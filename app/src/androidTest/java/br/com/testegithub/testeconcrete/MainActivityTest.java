package br.com.testegithub.testeconcrete;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

import br.com.testegithub.testeconcrete.controller.MainActivity;
import br.com.testegithub.testeconcrete.controller.PullRequestActivity;
import br.com.testegithub.testeconcrete.entity.Constants;

/**
 * Created by rodrigo on 18/08/2016.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private Solo solo;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    public void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
    }


    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }

    public void testMainActivity() throws Exception {
        solo.assertCurrentActivity("main activity", MainActivity.class);
    }

    public void testListItemClickshouldPullRequest() throws Exception{
        RecyclerView myRecyclerView = (RecyclerView) solo.getView(R.id.activity_main_rv_listgit);
        solo.waitForView(myRecyclerView);
        solo.clickInRecyclerView(1);
        solo.waitForActivity(PullRequestActivity.class,3000);
        solo.assertCurrentActivity("pull request activity", PullRequestActivity.class);
    }

    public void testListItemScroll() throws Exception {
        RecyclerView myRecyclerView = (RecyclerView) solo.getView(R.id.activity_main_rv_listgit);
        solo.waitForView(myRecyclerView);
        solo.scrollDownRecyclerView(2);
    }
}
