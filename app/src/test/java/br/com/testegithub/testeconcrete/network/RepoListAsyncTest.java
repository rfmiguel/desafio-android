package br.com.testegithub.testeconcrete.network;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import br.com.testegithub.testeconcrete.BuildConfig;
import br.com.testegithub.testeconcrete.entity.RepoListAsyncResult;
import br.com.testegithub.testeconcrete.entity.ServiceResult;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by rodrigo on 18/08/2016.
 */
public class RepoListAsyncTest {
    private RepoListAsync repoListAsync;
    private final ServiceClient<RepoListAsyncResult> serviceClient = Mockito.mock(ServiceClient.class);
    private final OnServiceCall<RepoListAsyncResult> onServiceCall = Mockito.mock(OnServiceCall.class);

    @Before
    public void setUp() {
        this.repoListAsync = new RepoListAsyncImpl(this.serviceClient);
    }

    @Test
    public void testGetRepoList() {
        int page = 1;
        this.repoListAsync.getRepoList(page,this.onServiceCall);
        verify(this.serviceClient).get(eq(BuildConfig.URL_GITHUB + String.format(RepoListAsyncImpl.URL_SEARCH,page)), eq((OnTransformResult<RepoListAsyncResult>) this.repoListAsync), eq(this.onServiceCall));
    }

    @Test
    public void testJsonParser() throws IOException {
        String jsonResponse = "{\n" +
                "  \"total_count\": 2213149,\n" +
                "  \"incomplete_results\": false,\n" +
                "  \"items\": [\n" +
                "    {\n" +
                "      \"id\": 29028775,\n" +
                "      \"name\": \"react-native\",\n" +
                "      \"full_name\": \"facebook/react-native\",\n" +
                "      \"owner\": {\n" +
                "        \"login\": \"facebook\",\n" +
                "        \"id\": 69631,\n" +
                "        \"avatar_url\": \"https://avatars.githubusercontent.com/u/69631?v=3\",\n" +
                "        \"gravatar_id\": \"\",\n" +
                "        \"url\": \"https://api.github.com/users/facebook\",\n" +
                "        \"html_url\": \"https://github.com/facebook\",\n" +
                "        \"followers_url\": \"https://api.github.com/users/facebook/followers\",\n" +
                "        \"following_url\": \"https://api.github.com/users/facebook/following{/other_user}\",\n" +
                "        \"gists_url\": \"https://api.github.com/users/facebook/gists{/gist_id}\",\n" +
                "        \"starred_url\": \"https://api.github.com/users/facebook/starred{/owner}{/repo}\",\n" +
                "        \"subscriptions_url\": \"https://api.github.com/users/facebook/subscriptions\",\n" +
                "        \"organizations_url\": \"https://api.github.com/users/facebook/orgs\",\n" +
                "        \"repos_url\": \"https://api.github.com/users/facebook/repos\",\n" +
                "        \"events_url\": \"https://api.github.com/users/facebook/events{/privacy}\",\n" +
                "        \"received_events_url\": \"https://api.github.com/users/facebook/received_events\",\n" +
                "        \"type\": \"Organization\",\n" +
                "        \"site_admin\": false\n" +
                "      },\n" +
                "      \"private\": false,\n" +
                "      \"html_url\": \"https://github.com/facebook/react-native\",\n" +
                "      \"description\": \"A framework for building native apps with React.\",\n" +
                "      \"fork\": false,\n" +
                "      \"url\": \"https://api.github.com/repos/facebook/react-native\",\n" +
                "      \"forks_url\": \"https://api.github.com/repos/facebook/react-native/forks\",\n" +
                "      \"keys_url\": \"https://api.github.com/repos/facebook/react-native/keys{/key_id}\",\n" +
                "      \"collaborators_url\": \"https://api.github.com/repos/facebook/react-native/collaborators{/collaborator}\",\n" +
                "      \"teams_url\": \"https://api.github.com/repos/facebook/react-native/teams\",\n" +
                "      \"hooks_url\": \"https://api.github.com/repos/facebook/react-native/hooks\",\n" +
                "      \"issue_events_url\": \"https://api.github.com/repos/facebook/react-native/issues/events{/number}\",\n" +
                "      \"events_url\": \"https://api.github.com/repos/facebook/react-native/events\",\n" +
                "      \"assignees_url\": \"https://api.github.com/repos/facebook/react-native/assignees{/user}\",\n" +
                "      \"branches_url\": \"https://api.github.com/repos/facebook/react-native/branches{/branch}\",\n" +
                "      \"tags_url\": \"https://api.github.com/repos/facebook/react-native/tags\",\n" +
                "      \"blobs_url\": \"https://api.github.com/repos/facebook/react-native/git/blobs{/sha}\",\n" +
                "      \"git_tags_url\": \"https://api.github.com/repos/facebook/react-native/git/tags{/sha}\",\n" +
                "      \"git_refs_url\": \"https://api.github.com/repos/facebook/react-native/git/refs{/sha}\",\n" +
                "      \"trees_url\": \"https://api.github.com/repos/facebook/react-native/git/trees{/sha}\",\n" +
                "      \"statuses_url\": \"https://api.github.com/repos/facebook/react-native/statuses/{sha}\",\n" +
                "      \"languages_url\": \"https://api.github.com/repos/facebook/react-native/languages\",\n" +
                "      \"stargazers_url\": \"https://api.github.com/repos/facebook/react-native/stargazers\",\n" +
                "      \"contributors_url\": \"https://api.github.com/repos/facebook/react-native/contributors\",\n" +
                "      \"subscribers_url\": \"https://api.github.com/repos/facebook/react-native/subscribers\",\n" +
                "      \"subscription_url\": \"https://api.github.com/repos/facebook/react-native/subscription\",\n" +
                "      \"commits_url\": \"https://api.github.com/repos/facebook/react-native/commits{/sha}\",\n" +
                "      \"git_commits_url\": \"https://api.github.com/repos/facebook/react-native/git/commits{/sha}\",\n" +
                "      \"comments_url\": \"https://api.github.com/repos/facebook/react-native/comments{/number}\",\n" +
                "      \"issue_comment_url\": \"https://api.github.com/repos/facebook/react-native/issues/comments{/number}\",\n" +
                "      \"contents_url\": \"https://api.github.com/repos/facebook/react-native/contents/{+path}\",\n" +
                "      \"compare_url\": \"https://api.github.com/repos/facebook/react-native/compare/{base}...{head}\",\n" +
                "      \"merges_url\": \"https://api.github.com/repos/facebook/react-native/merges\",\n" +
                "      \"archive_url\": \"https://api.github.com/repos/facebook/react-native/{archive_format}{/ref}\",\n" +
                "      \"downloads_url\": \"https://api.github.com/repos/facebook/react-native/downloads\",\n" +
                "      \"issues_url\": \"https://api.github.com/repos/facebook/react-native/issues{/number}\",\n" +
                "      \"pulls_url\": \"https://api.github.com/repos/facebook/react-native/pulls{/number}\",\n" +
                "      \"milestones_url\": \"https://api.github.com/repos/facebook/react-native/milestones{/number}\",\n" +
                "      \"notifications_url\": \"https://api.github.com/repos/facebook/react-native/notifications{?since,all,participating}\",\n" +
                "      \"labels_url\": \"https://api.github.com/repos/facebook/react-native/labels{/name}\",\n" +
                "      \"releases_url\": \"https://api.github.com/repos/facebook/react-native/releases{/id}\",\n" +
                "      \"deployments_url\": \"https://api.github.com/repos/facebook/react-native/deployments\",\n" +
                "      \"created_at\": \"2015-01-09T18:10:16Z\",\n" +
                "      \"updated_at\": \"2016-08-18T16:32:05Z\",\n" +
                "      \"pushed_at\": \"2016-08-18T15:53:25Z\",\n" +
                "      \"git_url\": \"git://github.com/facebook/react-native.git\",\n" +
                "      \"ssh_url\": \"git@github.com:facebook/react-native.git\",\n" +
                "      \"clone_url\": \"https://github.com/facebook/react-native.git\",\n" +
                "      \"svn_url\": \"https://github.com/facebook/react-native\",\n" +
                "      \"homepage\": \"http://facebook.github.io/react-native/\",\n" +
                "      \"size\": 121618,\n" +
                "      \"stargazers_count\": 36430,\n" +
                "      \"watchers_count\": 36430,\n" +
                "      \"language\": \"Java\",\n" +
                "      \"has_issues\": true,\n" +
                "      \"has_downloads\": true,\n" +
                "      \"has_wiki\": true,\n" +
                "      \"has_pages\": true,\n" +
                "      \"forks_count\": 7976,\n" +
                "      \"mirror_url\": null,\n" +
                "      \"open_issues_count\": 1109,\n" +
                "      \"forks\": 7976,\n" +
                "      \"open_issues\": 1109,\n" +
                "      \"watchers\": 36430,\n" +
                "      \"default_branch\": \"master\",\n" +
                "      \"score\": 1.0\n" +
                "}]}";
        RepoListAsyncResult repoListAsyncResult = ((OnTransformResult<RepoListAsyncResult>) this.repoListAsync).onTransformResult(jsonResponse);
        Assert.assertEquals(ServiceResult.SUCCESS, repoListAsyncResult.getServiceResult());
        Assert.assertEquals("react-native", repoListAsyncResult.getRepoList().get(0).getName());
    }

    @Test
    public void testJsonParserFail() throws IOException {
        String jsonResponse = "wrong JSON";
        RepoListAsyncResult repoListAsyncResult = ((OnTransformResult<RepoListAsyncResult>) this.repoListAsync).onTransformResult(jsonResponse);
        Assert.assertEquals(ServiceResult.FAIL, repoListAsyncResult.getServiceResult());
    }
}
