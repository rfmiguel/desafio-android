package br.com.testegithub.testeconcrete.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import br.com.testegithub.testeconcrete.entity.ErrorTypes;
import br.com.testegithub.testeconcrete.entity.PullRequestAsyncResult;
import br.com.testegithub.testeconcrete.entity.ServiceResult;
import br.com.testegithub.testeconcrete.model.delegate.PullRequestModelDelegate;
import br.com.testegithub.testeconcrete.network.OnServiceCall;
import br.com.testegithub.testeconcrete.network.PullRequestAsync;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by rodrigo on 18/08/2016.
 */
@SuppressWarnings("unchecked")
public class PullRequesModelTest {

    private PullRequestModelmpl pullrequestModel;
    private final PullRequestAsync pullRequestAsync = Mockito.mock(PullRequestAsync.class);
    private final PullRequestModelDelegate pullRequestModelDelegate = Mockito.mock(PullRequestModelDelegate.class);
    private final PullRequestAsyncResult pullRequestAsyncResult = Mockito.mock(PullRequestAsyncResult.class);
    private final ErrorTypes errorTypes = Mockito.mock(ErrorTypes.class);

    @Before
    public void setUp() {
        this.pullrequestModel = new PullRequestModelmpl(this.pullRequestAsync);
        this.pullrequestModel.setPullRequestModelDelegate(this.pullRequestModelDelegate);
    }

    @Test
    public void testGetPullRequest() {
        this.pullrequestModel.getPullRequest("facebook","react-native",1);
        verify(this.pullRequestAsync).getPullRequest(eq(1),eq("facebook"),eq("react-native"),any(OnServiceCall.class));
    }

    @Test
    public void testOnServiceCallFaill() {
        this.pullrequestModel.onServiceCallFail(this.errorTypes);
        verify(this.pullRequestModelDelegate).onGetPullRequestFail(Matchers.any(ErrorTypes.class));
    }

    @Test
    public void testOnServiceCallSuccess() {
        when(this.pullRequestAsyncResult.getServiceResult()).thenReturn(ServiceResult.SUCCESS);
        this.pullrequestModel.onServiceCallSuccess(this.pullRequestAsyncResult);
        verify(this.pullRequestAsyncResult).getPullRequestList();
        verify(this.pullRequestModelDelegate).onGetPullRequestOK();
    }

    @Test
    public void testOnServiceCallSuccessFail() {
        when(this.pullRequestAsyncResult.getServiceResult()).thenReturn(ServiceResult.FAIL);
        this.pullrequestModel.onServiceCallSuccess(this.pullRequestAsyncResult);
        verify(this.pullRequestAsyncResult).getPullRequestList();
        verify(this.pullRequestModelDelegate).onGetPullRequestFail(Matchers.any(ErrorTypes.class));
    }
}
