package br.com.testegithub.testeconcrete.network;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import br.com.testegithub.testeconcrete.BuildConfig;
import br.com.testegithub.testeconcrete.entity.PullRequestAsyncResult;
import br.com.testegithub.testeconcrete.entity.ServiceResult;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by rodrigo on 18/08/2016.
 */
@SuppressWarnings("unchecked")
public class PullRequestAsyncTest {

    private PullRequestAsync pullRequestAsync;
    private final ServiceClient<PullRequestAsyncResult> serviceClient = Mockito.mock(ServiceClient.class);
    private final OnServiceCall<PullRequestAsyncResult> onServiceCall = Mockito.mock(OnServiceCall.class);

    @Before
    public void setUp() {
        this.pullRequestAsync = new PullRequestAsyncImpl(this.serviceClient);
    }

    @Test
    public void testGetPullRequest() {
        String creator = "facebook";
        String repo = "react-native";
        int page = 1;
        this.pullRequestAsync.getPullRequest(page,creator,repo,this.onServiceCall);
        verify(this.serviceClient).get(eq(BuildConfig.URL_GITHUB + String.format(PullRequestAsyncImpl.URL_SEARCH,creator,repo,page)), eq((OnTransformResult<PullRequestAsyncResult>) this.pullRequestAsync), eq(this.onServiceCall));
    }

    @Test
    public void testJsonParser() throws IOException {
        String jsonResponse = "[\n" +
                "{\n" +
                "\"url\":\"https://api.github.com/repos/facebook/react-native/pulls/9456\",\n" +
                "\"id\":81718475,\n" +
                "\"html_url\":\"https://github.com/facebook/react-native/pull/9456\",\n" +
                "\"diff_url\":\"https://github.com/facebook/react-native/pull/9456.diff\",\n" +
                "\"patch_url\":\"https://github.com/facebook/react-native/pull/9456.patch\",\n" +
                "\"issue_url\":\"https://api.github.com/repos/facebook/react-native/issues/9456\",\n" +
                "\"number\":9456,\n" +
                "\"state\":\"open\",\n" +
                "\"locked\":false,\n" +
                "\"title\":\"[Android] Implement ScrollView sticky headers on Android\",\n" +
                "\"user\":{\n" +
                "\"login\":\"janicduplessis\",\n" +
                "\"id\":2677334,\n" +
                "\"avatar_url\":\"https://avatars.githubusercontent.com/u/2677334?v=3\",\n" +
                "\"gravatar_id\":\"\",\n" +
                "\"url\":\"https://api.github.com/users/janicduplessis\",\n" +
                "\"html_url\":\"https://github.com/janicduplessis\",\n" +
                "\"followers_url\":\"https://api.github.com/users/janicduplessis/followers\",\n" +
                "\"following_url\":\"https://api.github.com/users/janicduplessis/following{/other_user}\",\n" +
                "\"gists_url\":\"https://api.github.com/users/janicduplessis/gists{/gist_id}\",\n" +
                "\"starred_url\":\"https://api.github.com/users/janicduplessis/starred{/owner}{/repo}\",\n" +
                "\"subscriptions_url\":\"https://api.github.com/users/janicduplessis/subscriptions\",\n" +
                "\"organizations_url\":\"https://api.github.com/users/janicduplessis/orgs\",\n" +
                "\"repos_url\":\"https://api.github.com/users/janicduplessis/repos\",\n" +
                "\"events_url\":\"https://api.github.com/users/janicduplessis/events{/privacy}\",\n" +
                "\"received_events_url\":\"https://api.github.com/users/janicduplessis/received_events\",\n" +
                "\"type\":\"User\",\n" +
                "\"site_admin\":false\n" +
                "},\n" +
                "\"body\":\"This adds support for sticky headers on Android. The implementation if based primarily on the iOS one (https://github.com/facebook/react-native/blob/master/React/Views/RCTScrollView.m#L272) and adds some stuff that was missing to be able to handle z-index, view clipping, view hierarchy optimization and touch handling properly.\\r\\n\\r\\nSome notable changes:\\r\\n- Add `collapsableChildren` prop that applies collapsable for all direct children. This is necessary because we need to preserve the ScrollView's content native view hierarchy so sticky header indices refer to the right views. This felt like the best solution to avoid having to clone all child elements to add the collapsable prop.\\r\\n\\r\\n- Add `ReactHitTestView` interface to allow `ViewGroup` subclasses to handle touch event targets manually. This is necessary because we need to make sure touch events land on the sticky header instead of the view underneath it.\\r\\n\\r\\n- Add `ChildDrawingOrderDelegate` interface to allow changing the `ViewGroup` drawing order using `ViewGroup#getChildDrawingOrder`. This is used to change the content view drawing order to make sure headers are drawn over the other cells. Right now I'm only reversing the drawing order as drawing only the header views last added a lot of complexity especially because of view clipping and I don't think it should cause issues.\\r\\n\\r\\n- Take translate into consideration when clipping subviews.\\r\\n\\r\\n**Test plan**\\r\\nTested using the UIExplorer main menu on android.\\r\\nTested that touches get dispatched to the sticky header when over other touchable views.\\r\\nTested that touchables inside a sticky header work properly.\",\n" +
                "\"created_at\":\"2016-08-17T21:17:58Z\",\n" +
                "\"updated_at\":\"2016-08-18T02:45:36Z\",\n" +
                "\"closed_at\":null,\n" +
                "\"merged_at\":null,\n" +
                "\"merge_commit_sha\":\"4d4c0251d5577a5ea148134d2ba6deea1979afdd\",\n" +
                "\"assignee\":null,\n" +
                "\"assignees\":[\n" +
                "],\n" +
                "\"milestone\":null,\n" +
                "\"commits_url\":\"https://api.github.com/repos/facebook/react-native/pulls/9456/commits\",\n" +
                "\"review_comments_url\":\"https://api.github.com/repos/facebook/react-native/pulls/9456/comments\",\n" +
                "\"review_comment_url\":\"https://api.github.com/repos/facebook/react-native/pulls/comments{/number}\",\n" +
                "\"comments_url\":\"https://api.github.com/repos/facebook/react-native/issues/9456/comments\",\n" +
                "\"statuses_url\":\"https://api.github.com/repos/facebook/react-native/statuses/7767c127def0584f4292a3387fa83eb345450d63\",\n" +
                "\"head\":{\n" +
                "\"label\":\"janicduplessis:sticky-headers-android\",\n" +
                "\"ref\":\"sticky-headers-android\",\n" +
                "\"sha\":\"7767c127def0584f4292a3387fa83eb345450d63\",\n" +
                "\"user\":{\n" +
                "\"login\":\"janicduplessis\",\n" +
                "\"id\":2677334,\n" +
                "\"avatar_url\":\"https://avatars.githubusercontent.com/u/2677334?v=3\",\n" +
                "\"gravatar_id\":\"\",\n" +
                "\"url\":\"https://api.github.com/users/janicduplessis\",\n" +
                "\"html_url\":\"https://github.com/janicduplessis\",\n" +
                "\"followers_url\":\"https://api.github.com/users/janicduplessis/followers\",\n" +
                "\"following_url\":\"https://api.github.com/users/janicduplessis/following{/other_user}\",\n" +
                "\"gists_url\":\"https://api.github.com/users/janicduplessis/gists{/gist_id}\",\n" +
                "\"starred_url\":\"https://api.github.com/users/janicduplessis/starred{/owner}{/repo}\",\n" +
                "\"subscriptions_url\":\"https://api.github.com/users/janicduplessis/subscriptions\",\n" +
                "\"organizations_url\":\"https://api.github.com/users/janicduplessis/orgs\",\n" +
                "\"repos_url\":\"https://api.github.com/users/janicduplessis/repos\",\n" +
                "\"events_url\":\"https://api.github.com/users/janicduplessis/events{/privacy}\",\n" +
                "\"received_events_url\":\"https://api.github.com/users/janicduplessis/received_events\",\n" +
                "\"type\":\"User\",\n" +
                "\"site_admin\":false\n" +
                "},\n" +
                "\"repo\":{\n" +
                "\"id\":33167181,\n" +
                "\"name\":\"react-native\",\n" +
                "\"full_name\":\"janicduplessis/react-native\",\n" +
                "\"owner\":{\n" +
                "\"login\":\"janicduplessis\",\n" +
                "\"id\":2677334,\n" +
                "\"avatar_url\":\"https://avatars.githubusercontent.com/u/2677334?v=3\",\n" +
                "\"gravatar_id\":\"\",\n" +
                "\"url\":\"https://api.github.com/users/janicduplessis\",\n" +
                "\"html_url\":\"https://github.com/janicduplessis\",\n" +
                "\"followers_url\":\"https://api.github.com/users/janicduplessis/followers\",\n" +
                "\"following_url\":\"https://api.github.com/users/janicduplessis/following{/other_user}\",\n" +
                "\"gists_url\":\"https://api.github.com/users/janicduplessis/gists{/gist_id}\",\n" +
                "\"starred_url\":\"https://api.github.com/users/janicduplessis/starred{/owner}{/repo}\",\n" +
                "\"subscriptions_url\":\"https://api.github.com/users/janicduplessis/subscriptions\",\n" +
                "\"organizations_url\":\"https://api.github.com/users/janicduplessis/orgs\",\n" +
                "\"repos_url\":\"https://api.github.com/users/janicduplessis/repos\",\n" +
                "\"events_url\":\"https://api.github.com/users/janicduplessis/events{/privacy}\",\n" +
                "\"received_events_url\":\"https://api.github.com/users/janicduplessis/received_events\",\n" +
                "\"type\":\"User\",\n" +
                "\"site_admin\":false\n" +
                "},\n" +
                "\"private\":false,\n" +
                "\"html_url\":\"https://github.com/janicduplessis/react-native\",\n" +
                "\"description\":\"A framework for building native apps with React.\",\n" +
                "\"fork\":true,\n" +
                "\"url\":\"https://api.github.com/repos/janicduplessis/react-native\",\n" +
                "\"forks_url\":\"https://api.github.com/repos/janicduplessis/react-native/forks\",\n" +
                "\"keys_url\":\"https://api.github.com/repos/janicduplessis/react-native/keys{/key_id}\",\n" +
                "\"collaborators_url\":\"https://api.github.com/repos/janicduplessis/react-native/collaborators{/collaborator}\",\n" +
                "\"teams_url\":\"https://api.github.com/repos/janicduplessis/react-native/teams\",\n" +
                "\"hooks_url\":\"https://api.github.com/repos/janicduplessis/react-native/hooks\",\n" +
                "\"issue_events_url\":\"https://api.github.com/repos/janicduplessis/react-native/issues/events{/number}\",\n" +
                "\"events_url\":\"https://api.github.com/repos/janicduplessis/react-native/events\",\n" +
                "\"assignees_url\":\"https://api.github.com/repos/janicduplessis/react-native/assignees{/user}\",\n" +
                "\"branches_url\":\"https://api.github.com/repos/janicduplessis/react-native/branches{/branch}\",\n" +
                "\"tags_url\":\"https://api.github.com/repos/janicduplessis/react-native/tags\",\n" +
                "\"blobs_url\":\"https://api.github.com/repos/janicduplessis/react-native/git/blobs{/sha}\",\n" +
                "\"git_tags_url\":\"https://api.github.com/repos/janicduplessis/react-native/git/tags{/sha}\",\n" +
                "\"git_refs_url\":\"https://api.github.com/repos/janicduplessis/react-native/git/refs{/sha}\",\n" +
                "\"trees_url\":\"https://api.github.com/repos/janicduplessis/react-native/git/trees{/sha}\",\n" +
                "\"statuses_url\":\"https://api.github.com/repos/janicduplessis/react-native/statuses/{sha}\",\n" +
                "\"languages_url\":\"https://api.github.com/repos/janicduplessis/react-native/languages\",\n" +
                "\"stargazers_url\":\"https://api.github.com/repos/janicduplessis/react-native/stargazers\",\n" +
                "\"contributors_url\":\"https://api.github.com/repos/janicduplessis/react-native/contributors\",\n" +
                "\"subscribers_url\":\"https://api.github.com/repos/janicduplessis/react-native/subscribers\",\n" +
                "\"subscription_url\":\"https://api.github.com/repos/janicduplessis/react-native/subscription\",\n" +
                "\"commits_url\":\"https://api.github.com/repos/janicduplessis/react-native/commits{/sha}\",\n" +
                "\"git_commits_url\":\"https://api.github.com/repos/janicduplessis/react-native/git/commits{/sha}\",\n" +
                "\"comments_url\":\"https://api.github.com/repos/janicduplessis/react-native/comments{/number}\",\n" +
                "\"issue_comment_url\":\"https://api.github.com/repos/janicduplessis/react-native/issues/comments{/number}\",\n" +
                "\"contents_url\":\"https://api.github.com/repos/janicduplessis/react-native/contents/{+path}\",\n" +
                "\"compare_url\":\"https://api.github.com/repos/janicduplessis/react-native/compare/{base}...{head}\",\n" +
                "\"merges_url\":\"https://api.github.com/repos/janicduplessis/react-native/merges\",\n" +
                "\"archive_url\":\"https://api.github.com/repos/janicduplessis/react-native/{archive_format}{/ref}\",\n" +
                "\"downloads_url\":\"https://api.github.com/repos/janicduplessis/react-native/downloads\",\n" +
                "\"issues_url\":\"https://api.github.com/repos/janicduplessis/react-native/issues{/number}\",\n" +
                "\"pulls_url\":\"https://api.github.com/repos/janicduplessis/react-native/pulls{/number}\",\n" +
                "\"milestones_url\":\"https://api.github.com/repos/janicduplessis/react-native/milestones{/number}\",\n" +
                "\"notifications_url\":\"https://api.github.com/repos/janicduplessis/react-native/notifications{?since,all,participating}\",\n" +
                "\"labels_url\":\"https://api.github.com/repos/janicduplessis/react-native/labels{/name}\",\n" +
                "\"releases_url\":\"https://api.github.com/repos/janicduplessis/react-native/releases{/id}\",\n" +
                "\"deployments_url\":\"https://api.github.com/repos/janicduplessis/react-native/deployments\",\n" +
                "\"created_at\":\"2015-03-31T05:43:42Z\",\n" +
                "\"updated_at\":\"2016-08-13T18:57:25Z\",\n" +
                "\"pushed_at\":\"2016-08-17T20:55:33Z\",\n" +
                "\"git_url\":\"git://github.com/janicduplessis/react-native.git\",\n" +
                "\"ssh_url\":\"git@github.com:janicduplessis/react-native.git\",\n" +
                "\"clone_url\":\"https://github.com/janicduplessis/react-native.git\",\n" +
                "\"svn_url\":\"https://github.com/janicduplessis/react-native\",\n" +
                "\"homepage\":\"http://facebook.github.io/react-native/\",\n" +
                "\"size\":49210,\n" +
                "\"stargazers_count\":0,\n" +
                "\"watchers_count\":0,\n" +
                "\"language\":\"Java\",\n" +
                "\"has_issues\":false,\n" +
                "\"has_downloads\":true,\n" +
                "\"has_wiki\":true,\n" +
                "\"has_pages\":false,\n" +
                "\"forks_count\":0,\n" +
                "\"mirror_url\":null,\n" +
                "\"open_issues_count\":0,\n" +
                "\"forks\":0,\n" +
                "\"open_issues\":0,\n" +
                "\"watchers\":0,\n" +
                "\"default_branch\":\"master\"\n" +
                "}\n" +
                "},\n" +
                "\"base\":{},\n" +
                "\"_links\":{}\n" +
                "}]";
        PullRequestAsyncResult pullRequestAsyncResult = ((OnTransformResult<PullRequestAsyncResult>) this.pullRequestAsync).onTransformResult(jsonResponse);
        Assert.assertEquals(ServiceResult.SUCCESS, pullRequestAsyncResult.getServiceResult());
        Assert.assertEquals("[Android] Implement ScrollView sticky headers on Android", pullRequestAsyncResult.getPullRequestList().get(0).getTitle());
    }

    @Test
    public void testJsonParserFail() throws IOException {
        String jsonResponse = "wrong JSON";
        PullRequestAsyncResult pullRequestAsyncResult = ((OnTransformResult<PullRequestAsyncResult>) this.pullRequestAsync).onTransformResult(jsonResponse);
        Assert.assertEquals(ServiceResult.FAIL, pullRequestAsyncResult.getServiceResult());
    }
}
