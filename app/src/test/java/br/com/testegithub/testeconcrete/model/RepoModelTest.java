package br.com.testegithub.testeconcrete.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import br.com.testegithub.testeconcrete.entity.ErrorTypes;
import br.com.testegithub.testeconcrete.entity.RepoListAsyncResult;
import br.com.testegithub.testeconcrete.entity.ServiceResult;
import br.com.testegithub.testeconcrete.model.delegate.RepoModelDelegate;
import br.com.testegithub.testeconcrete.network.OnServiceCall;
import br.com.testegithub.testeconcrete.network.RepoListAsync;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by rodrigo on 18/08/2016.
 */
public class RepoModelTest {

    private RepoModelImpl repoModel;
    private final RepoListAsync repoListAsync = Mockito.mock(RepoListAsync.class);
    private final RepoModelDelegate repoModelDelegate = Mockito.mock(RepoModelDelegate.class);
    private final RepoListAsyncResult repoListAsyncResult = Mockito.mock(RepoListAsyncResult.class);
    private final ErrorTypes errorTypes = Mockito.mock(ErrorTypes.class);

    @Before
    public void setUp() {
        this.repoModel = new RepoModelImpl(this.repoListAsync);
        this.repoModel.setRepoModelDelegate(this.repoModelDelegate);
    }

    @Test
    public void testGetPullRequest() {
        this.repoModel.getRepos(1);
        verify(this.repoListAsync).getRepoList(eq(1),any(OnServiceCall.class));
    }

    @Test
    public void testOnServiceCallFaill() {
        this.repoModel.onServiceCallFail(this.errorTypes);
        verify(this.repoModelDelegate).onGetReposFail(Matchers.any(ErrorTypes.class));
    }

    @Test
    public void testOnServiceCallSuccess() {
        when(this.repoListAsyncResult.getServiceResult()).thenReturn(ServiceResult.SUCCESS);
        this.repoModel.onServiceCallSuccess(this.repoListAsyncResult);
        verify(this.repoListAsyncResult).getRepoList();
        verify(this.repoModelDelegate).onGetReposOK();
    }

    @Test
    public void testOnServiceCallSuccessFail() {
        when(this.repoListAsyncResult.getServiceResult()).thenReturn(ServiceResult.FAIL);
        this.repoModel.onServiceCallSuccess(this.repoListAsyncResult);
        verify(this.repoListAsyncResult).getRepoList();
        verify(this.repoModelDelegate).onGetReposFail(Matchers.any(ErrorTypes.class));
    }
}
